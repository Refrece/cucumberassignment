package stepdefinitionfolder;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoWebPageSteps {
static WebDriver driver=null;
@Given("Open the Demo Web Shop")
public void open_the_demo_web_shop() {
	driver=new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
}

@Then("The Home Page of the website get displayed")
public void the_home_page_of_the_website_get_displayed() {
	System.out.println("Home page displayed");
}

@When("Click on the Login button")
public void click_on_the_login_button() {
	driver.findElement(By.xpath("//a[text()='Log in']")).click();
}

@When("Use should enter the valid Username")
public void use_should_enter_the_valid_username() {
   driver.findElement(By.id("Email")).sendKeys("tkarthi6379@gmail.com");
}

@When("User enter the valid Password")
public void user_enter_the_valid_password() {
	driver.findElement(By.id("Password")).sendKeys("Karthi@1213");
}
@When("Click on the Login button after enter the username and password")
public void click_on_the_login_button_after_enter_the_username_and_password() {
   driver.findElement(By.xpath("//input[@value='Log in']")).click();
}
@Then("The home page should be display with login Username")
public void the_home_page_should_be_display_with_login_username() {
	System.out.println("Long in with name");
}

@Then("Click on the Logout button")
public void click_on_the_logout_button() {
	driver.findElement(By.xpath("//a[text()='Log out']")).click();
}

@Then("Again the home page should be displayed")
public void again_the_home_page_should_be_displayed() {
	System.out.println("Again the home page should be displayed");
}

@Then("Close the browser")
public void close_the_browser() {
	driver.close();
}
}
