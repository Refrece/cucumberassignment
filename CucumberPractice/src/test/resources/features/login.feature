Feature: Demo Web Shop Validation

Scenario: To Verify The Demo Web Shop Login Functionality
  Given Open the Demo Web Shop
  Then The Home Page of the website get displayed
  When Click on the Login button
  And Use should enter the valid Username
  And User enter the valid Password
  And Click on the Login button after enter the username and password 
  Then The home page should be display with login Username
  And Click on the Logout button
  Then Again the home page should be displayed
  And Close the browser


